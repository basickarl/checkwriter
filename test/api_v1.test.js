/* eslint prefer-arrow-callback: 0, func-names: 0 */

const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;
const should = require('chai').should();

const config = require('application/lib/config');

const server = require('application/server');

chai.use(chaiHttp);

describe('Api v1', function () {
    before(function (done) {
        server.start()
            .then(() => {
                done();
            })
            .catch((error) => {
                console.error(error);
            });
    });

    describe('Health', function () {
        it('should recieve health status alive', function (done) {
            chai.request(`http://${config.get('http.host')}:${config.get('http.port')}`)
                .get('/api/v1/health')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.have.header('content-type', 'application/vnd.api+json');
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.status.should.equal('alive');
                    res.body.should.have.property('v');
                    res.body.v.should.equal('1');
                    done();
                });
        });
    });
});
