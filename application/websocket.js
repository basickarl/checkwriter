// Logger
const {
    ExecutionTime,
    log
} = require('application/lib/log');

const executionTime = new ExecutionTime();

const config = require('application/lib/config');

const { locales } = require('application/lib/locales');

const socketIo = require('socket.io');

const socketUsers = {};

module.exports = (server) => {
    const io = socketIo(server, config.get('socketIo'));

    io.on('connection', async (socket) => {
        log.info(`Client connection > ${socket.id}`);

        socketUsers[socket.id] = socket.id;

        socket
            .on('disconnect', () => {
                log.info(`Client disconnect > ${socket.id}`);
                delete socketUsers[socket.id];
            })
            .on('locale', (data) => {
                log.info(`Client locale > ${JSON.stringify(data)}`);

                const locale = locales[`${data.code}.json`];

                socket.emit('locale', locale);
                log.info(`Client locale < ${JSON.stringify(locale)}`);
            });
    });
};

log.info('Websockets initialized');
log.trace(`Websockets initialized execution time: ${executionTime.finish()}ms`);
