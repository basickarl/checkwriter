const fs = require('fs');
const geoip = require('geoip-lite');

// Logger
const {
    ExecutionTime,
    log
} = require('application/lib/log');

const executionTime = new ExecutionTime();

const locales = {};

function loadLocales () {
    const files = fs.readdirSync('application/locales');
    files.forEach((file) => {
        try {
            locales[file] = JSON.parse(fs.readFileSync(`application/locales/${file}`, 'utf8'));
        } catch (error) {
            error.message += `{error.message} file: application/locales/${file}`;
            throw error;
        }
    });
    return locales;
}

function getLocaleFromIp (ip) {
    const geo = geoip.lookup(ip);
    if (!geo) {
        return locales['en_GB.json'];
    }
    const locale = locales[Object.keys(locales).find(key => key.indexOf(geo.country) !== -1)];
    if (!locale) {
        return locales['en_GB.json'];
    }
    return locale;
}

function setLocaleCookie (code, cookies) {
    cookies.set('locale', code, {
        signed: false,
        // secure: config.cookies.secure, // TODO: edit in production
        expires: 0, // persistant
        httpOnly: false // editable client side
    });
}

// Used in the footer
function getLocaleInputList (code) {
    return Object.keys(locales).map((key) => {
        const locale = {
            language: locales[key].language,
            code: locales[key].code
        };
        if (key.indexOf(code) !== -1) {
            locale.selected = true;
        }
        return locale;
    });
}

// Used when user is not signed in, otherwise the locale is taken from the users profile
function getLocaleFromCookie (cookies) {
    const code = cookies.get('locale');
    if (!code) { // cookie has not been set
        return null;
    }
    const locale = locales[Object.keys(locales).find(key => key.indexOf(code) !== -1)];
    if (!locale) { // locale doesn't exist on the server
        return null;
    }
    return locale;
}

loadLocales();

module.exports = {
    getLocaleFromIp,
    getLocaleFromCookie,
    setLocaleCookie,
    getLocaleInputList,
    loadLocales,
    locales
};

log.info('Locales lib initialized');
log.trace(`Locales lib initialized execution time: ${executionTime.finish()}ms`);
