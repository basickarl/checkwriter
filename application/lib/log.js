const retardlog = require('retardlog');

function ExecutionTime () {
    this.startTime = process.hrtime();
    this.finish = function finish () {
        const totalTime = process.hrtime(this.startTime);
        const totalMilliseconds = totalTime[1] / 1000000;
        return totalMilliseconds;
    };
}

const executionTime = new ExecutionTime();

const log = retardlog.create([
    {
        type: 'file',
        path: 'logs/log.log'
    },
    {
        type: 'console'
    }
]);

module.exports = {
    log,
    ExecutionTime
};

log.info('Log lib initialized');
log.trace(`Log lib initialized execution time: ${executionTime.finish()}ms`);
