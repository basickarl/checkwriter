// Logger
const {
    ExecutionTime,
    log
} = require('application/lib/log');

const executionTime = new ExecutionTime();

const fs = require('fs');

const convict = require('convict');

// Schema
const schema = {
    env: {
        doc: 'The applicaton environment.',
        format: ['production', 'development', 'test'],
        default: 'development',
        env: 'NODE_ENV'
    },
    http: {
        host: {
            doc: 'The host address to bind.',
            format: 'String',
            default: '127.0.0.1',
            env: 'NODE_HOST'
        },
        port: {
            doc: 'The port to bind',
            format: 'port',
            default: 8080,
            env: 'NODE_PORT'
        },
        https: {
            doc: 'Does the server use https.',
            format: 'Boolean',
            default: false,
            env: 'NODE_HTTPS'
        }
    },
    cookies: {
        keys: {
            doc: 'Set signed cookies keys.',
            format: Array,
            default: ['secr3t']
        }
    },
    isProduction: {
        default: false
    },
    socketIo: {
        options: {
            forceNew: {
                format: 'Boolean',
                default: true
            },
            cookies: {
                format: 'Boolean',
                default: false
            },
            secure: {
                format: 'Boolean',
                default: false
            }
        },
        timeout: {
            format: 'int',
            default: 5000
        }
    }
};

// Create convict config
const config = convict(schema);

// Recursive function to set leaf properties to the default value
function setRecursiveLeaf (object) {
    const exceptions = ['env', 'isProduction'];
    Object.keys(object).forEach((key) => {
        if (exceptions.indexOf(key) !== -1) {
            delete object[key];
        } else if (Object.prototype.hasOwnProperty.call(object[key], 'default')) { // hasDefaultKey
            // Replace the keys properties with the default property value
            object[key] = object[key].default;
        } else {
            setRecursiveLeaf(object[key]);
        }
    });
}

// Generate an example json config file to the config/example.json file
config.generateConfig = () => {
    const exampleJson = Object.assign({}, schema);
    setRecursiveLeaf(exampleJson);

    // Make configs directory if it does not exist
    if (!fs.existsSync('configs')) {
        fs.mkdirSync('configs');
    }

    fs.writeFileSync('configs/example.json', JSON.stringify(exampleJson, null, 2), 'utf-8');
    console.log('* example json config file written to configs/example.json');
};

// Generate an example json config file to the config/example.json file
config.loadConfig = () => {
    // Load environment dependent configuration
    const env = config.get('env');

    // Set isProduction
    config.set('isProduction', env === 'production');

    if (fs.existsSync(`configs/${env}.json`)) {
        config.loadFile(`configs/${env}.json`);
        // Perform validation
        config.validate({ allowed: 'strict' });
        console.log(`* loaded configs/${env}.json configuration file`);
    } else {
        console.log(`* the configuration file configs/${env}.json does not exist, loading default configuration`);
    }

    config.set('hasTraceLevel',
        (config.get('log.console.levels').filter((level) => {
            if (level === 'trace') {
                return true;
            }
            return false;
        })).length > 0);
};

// TODO: Set a new configuration at runtime
config.setConfig = (newConfig) => {

};

// Exporting object because this is how Convict works
module.exports = config;

log.info('Config lib initialized');
log.trace(`Config lib initialized execution time: ${executionTime.finish()}ms`);
