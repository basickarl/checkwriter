const fs = require('fs');
const os = require('os');
const player = require('node-wav-player');
const util = require('util');
const native2ascii = require('node-native2ascii');
const urlencode = require('urlencode');
const ffmpeg = require('fluent-ffmpeg');
const log = require('application/libs/log');
const request = require('request');

const osPlatform = os.platform();

const forvoKey = '70b876e5bad1982c23b1825640a72ca7';
const forvoLanguageCodes = {
    russian: {
        isoCode: 'ru',
        englishName: 'Russian',
        nativeName: 'Русский'
    },
    swedish: {
        isoCode: 'sv',
        englishName: 'Swedish',
        nativeName: 'Svenska'
    }
};

function readJsonFile(filePath) {
    const jsonObject = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    return jsonObject;
}

function writeJsonFile(filePath, json) {
    fs.writeFileSync(filePath, json, 'utf8');
}

async function playWord(word, language) {
    if (osPlatform === 'win32') {
        const wavWordCachePath = `word_mp3_cache/${language}_${word}.wav`;
        // Windows must play a wav file, not mp3
        let wavFileExists = fs.existsSync(wavWordCachePath);
        if (!wavFileExists) { // Does not exist, must convert
            // Convert to wav
            await new Promise((resolve, reject) => {
                const mp3WordCachePath = `word_mp3_cache/${language}_${word}.mp3`;
                ffmpeg(mp3WordCachePath)
                    .toFormat('wav')
                    .on('error', (error) => {
                        reject(error);
                    })
                    .on('progress', (progress) => {
                        log.info('Processing: ' + progress.targetSize + ' KB converted');
                    })
                    .on('end', () => {
                        log.info('Finished converting');
                        resolve();
                    })
                    .save(wavWordCachePath);
            });

            wavFileExists = fs.existsSync(wavWordCachePath);
            if (!wavFileExists) { // Check to see now it exists
                log.error(new Error('The wav file did not convert correctly'));
                process.exit(0);
            }
        }
    }

    // Play file
    try {
        if (osPlatform === 'linux' || osPlatform === 'darwin') {
    
        } else if (osPlatform === 'win32') {
            await player.play({
                path: wavWordCachePath,
            });
        }
        log.info('The wav file started to be played successfully');
    } catch (error) {
        
    }
}

async function downloadWord(word, language) {
    const timestamp = new Date().getTime();
    const languageIsoCode = forvoLanguageCodes[language].isoCode;
    // Get word information first
    const urlEncodedWord = urlencode(word);
    const wordInfoUrl = `https://apifree.forvo.com/key/${forvoKey}/format/json/action/standard-pronunciation/word/${urlEncodedWord}/language/${languageIsoCode}`;
    log.info(`Requesting: ${wordInfoUrl}`);
    const bodyString = await new Promise((resolve, reject) => {
        request(wordInfoUrl, (error, response, body) => {
            if (error) {
                reject(error);
            }
            resolve(body);
        });
    });
    const body = JSON.parse(bodyString);
    log.info('Response: ');
    console.log(body); // TODO: Fix retardlog for JSON objects
    /*
        { items:
            [ { id: 237706,
                word: 'привет',
                original: 'привет',
                addtime: '2009-06-28 13:14:47',
                hits: 31450,
                username: 'Finanwen',
                sex: 'f',
                country: 'Ukraine',
                code: 'ru',
                langname: 'Russian',
                pathmp3: 'https://apifree.forvo.com/audio/373d2a292q3o1h251o2g3o361j1m213l3o2a283a3329362i2i3n3g26343e1n1p3e3l211j25321f3n331n3c3c1b3p38232l2d3i2824213j1p3a213q262p1o3c22393n2621342j291j361b36313o1k2a291k2b3c2o223n1t1t_2j361l1m3j3m2c38333m2g2n283p3b1m2m222e282h371t1t',
                pathogg: 'https://apifree.forvo.com/audio/361k3o392k391g371f1f3o2o2i3q2q3f2q1j231b2i3f1k3h1k1i1o1p312a2f1o253q2a1b2e3i2g3b3l343b3n262334231o263o3f1n1n3d282i2o1f3923372p3l311i2i243f373e1i283o2d2j312l3c2d233q342b25371t1t_3q1b2g1b3a2d2f3o3h292225323o3l241o3h2h3624211t1t',
                rate: 8,
                num_votes: 20,
                num_positive_votes: 14 } ] }
    */
    // After word information, download mp3
    const isInvalidResponse = (!body.items);
    if (isInvalidResponse) {
        log.error(new Error('Body response from word info url was either invalid or empty:'), body);
        process.exit(1);
    }
    if (body.items.length === 0) {
        throw new Error(`the word ${word} does not exist in the language ${language}`);
    }

    const wordMp3Url = body.items[0].pathmp3;
    log.info(`Requesting: ${wordMp3Url}`);
    const mp3WordCachePath = `word_mp3_cache/${language}_${word}.mp3`;
    await new Promise((resolve, reject) => {
        const stream = request
            .get(wordMp3Url)
            .on('error', (error) => {
                log.error(error);
                process.exit(1);
            })
            .pipe(fs.createWriteStream(mp3WordCachePath))
        stream.on('finish', resolve);
    });

    mp3FileExists = fs.existsSync(mp3WordCachePath);
    if (!mp3FileExists) { // Check to see now it exists
        log.error(new Error('The mp3 file did not download correctly'));
        process.exit(0);
    }
}

module.exports = {
    readJsonFile,
    writeJsonFile,
    playWord,
    downloadWord,
    getUser,
};