
const appNotificationText = require('application/client/components/d__notification-text');

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  // eslint-disable-line no-useless-escape
    return re.test(email);
}

// Make sure you capitalize the property name (Transform instead of transform)
function setVendor(element, property, value) {
    element.style[`webkit${property}`] = value;
    element.style[`moz${property}`] = value;
    element.style[`ms${property}`] = value;
    element.style[`o${property}`] = value;
}

function autocomplete(array, input) {
    if (!input) {
        return [];
    }
    const newArray = array.filter((obj) => {
        const doesContainString = Object.values(obj).reduce((accumulator, value) => {
            if (accumulator
                || value.toLowerCase().indexOf(input.toLowerCase()) !== -1) {
                return true;
            }
            return false;
        }, false);
        return doesContainString;
    });
    return newArray;
}

async function notificationText(body, timeout = true) {
    const result = await appNotificationText.render({ // TODO: Fix naming convention for this
        body,
        timeout,
    });
    result.appendTo(document.body);
}

module.exports = {
    autocomplete,
    setVendor,
    validateEmail,
    notificationText,
};


    /*
    this.clickThrottle = throttle(() => {
      console.log('-------------------------------- throttle!');
    }, this.clickThrottle, 2000)();
    */

    // function throttle(callback, wait, context = this) { // TODO: Make more pretty
//   let timeout = null;
//   let callbackArgs = null;
//   const later = () => {
//     callback.apply(context, callbackArgs)
//     timeout = null;
//   };
//   return () => {
//     if (!timeout) {
//       callbackArgs = arguments;
//       timeout = setTimeout(later, wait);
//     }
//   };
// }