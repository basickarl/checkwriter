/* eslint no-underscore-dangle: 0 */

require('regenerator-runtime/runtime'); // Needed for async/await transform (es2017)

// Local
const {
    loadState
} = require('application/client/store');
require('application/client/websocket');

// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__;

// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__;

loadState(preloadedState);

// Removes Facebooks sign in hash
if (window.location.hash === '#_=_') {
    history.replaceState // eslint-disable-line no-unused-expressions
        ?        history.replaceState(null, null, window.location.href.split('#')[0])
        : window.location.hash = '';
}

window.onload = function () {
    const elementPageLoad = document.getElementById('page-load');
    elementPageLoad.style = 'visibility: hidden; opacity: 0;';
    // setTimeout(() => {
    //     elementPageLoad.parentNode.removeChild(elementPageLoad);
    // }, 1 * 1000);
};

console.log('* index loaded');
