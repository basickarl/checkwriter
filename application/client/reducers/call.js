module.exports = (state = {}, action) => {
    // console.log('call state:', state, 'action:', action);
    if (action.type === 'UPDATE_PENDING') {
        return Object.assign(state, {
            fetching: true
        });
    } else if (action.type === 'UPDATE_FULFILLED') {
        return Object.assign(state, {
            fetching: false,
            fetched: true,
            data: action.payload
        });
    } else if (action.type === 'UPDATE_REJECTED') {
        return Object.assign(state, {
            fetching: false,
            error: action.payload
        });
    }
    return state;
};
