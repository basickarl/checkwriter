module.exports = (state = {}, action) => {
    // console.log('locale state:', state, 'action:', action);
    if (action.type === 'LOCALE') {
        const newState = action.payload;
        newState.locales = state.locales;

        newState.locales.forEach((locale) => {
            if (locale.selected
                && locale.locale === state.locale) {
                delete locale.selected;
            } else if (locale.locale === action.payload.locale) {
                locale.selected = true;
            }
        });

        return newState;
    }
    return state;
};
