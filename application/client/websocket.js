
const io = require('socket.io-client');

const { getStore } = require('application/client/store');

const socket = io();
socket
    .on('connect', () => {
        console.log('> connect');

        socket.emit('private', {});
    })
    .on('locale', (data) => {
        console.log('> locale', data);

        document.cookie =
            `locale=${data.code}; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/`;

        getStore().dispatch({
            type: 'LOCALE',
            payload: data,
        });
    });

module.exports = socket;

console.log('* websocket module initialized');
