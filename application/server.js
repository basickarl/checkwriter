const config = require('application/lib/config');

// Logger
const {
    ExecutionTime,
    log
} = require('application/lib/log');

const executionTime = new ExecutionTime();

// Initilize Marko & Lasso
require('marko/browser-refresh').enable();
require('lasso').configure({
    plugins: [
        // 'lasso-use-strict',
        'lasso-sass', // Allow SCSS files to be rendered to CSS
        'lasso-marko' // Allow Marko templates to be compiled and transported to the browser
    ],
    require: {
        transforms: [{
            transform: 'lasso-babel-transform',
            config: {
                // directly specify babel options
                babelOptions: {
                    presets: ['@babel/preset-env']
                }
            }
        }]
    }
});

// Node.js modules
const http = require('http');

// Local modules
const websockets = require('application/websocket');

// NPM modules
const compress = require('koa-compress');
const Koa = require('koa');
const mount = require('koa-mount');
const serve = require('koa-static');

const bodyParser = require('koa-bodyparser');

const app = new Koa();

app.keys = config.get('cookies.keys');

// Enable gzip compression for all HTTP responses
// app.use(compress({
//   threshold: 2048,
//   flush: require('zlib').Z_SYNC_FLUSH, // eslint-disable-line global-require
// }));

// Allow all of the generated files under "static" to be served
app.use(mount('/static', serve('static')));

app.use(bodyParser());

const {
    getLocaleInputList
} = require('application/lib/locales');
const localeReducer = require('application/client/reducers/locale');
const {
    combineReducers,
    createStore
} = require('redux');
const { getLocaleFromIp, getLocaleFromCookie, setLocaleCookie } = require('application/lib/locales');
app.use(async (ctx, next) => {
    // Get the correct locale for the user
    let locale = getLocaleFromCookie(ctx.cookies);
    if (!locale) { // locale from cookie did not work, try ip
        const ip = (ctx.req.headers['x-forwarded-for'] || '').split(',')[0] || ctx.ip;
        locale = getLocaleFromIp(ip);
        setLocaleCookie(locale.code, ctx.cookies);
    }
    ctx.locale = locale;

    // ctx.body = 'Hello World';

    let template;
    let reducersObject;

    template = require('application/client/pages/home/home.marko');
    reducersObject = {
        locale: localeReducer
    };

    // Setup preloaded state for the client side
    const reducers = combineReducers(reducersObject);
    const store = createStore(reducers);
    const preloadedState = store.getState();
    preloadedState.locale = {
        ...ctx.locale
    };
    // Used for selecting the language in the footer
    preloadedState.locale.locales = getLocaleInputList(ctx.locale.code);

    ctx.type = 'text/html';

    ctx.body = template.stream({
        $global: {
            serializedGlobals: {
                PRELOADED_STATE: true
            },
            PRELOADED_STATE: preloadedState
        },
        pages: preloadedState.locale.pages
    });

    return next();
});

const port = config.get('http.port');

function start () {
    return new Promise((resolve) => {
        const server = http.createServer(app.callback());

        websockets(server);

        server.listen(port, () => {
            log.info(`Listening to port ${port}`);

            if (process.send) {
                process.send({
                    event: 'online'
                });
            }

            resolve();
        });
    });
}

log.info('Server initialized');
log.trace(`Server initialized execution time: ${executionTime.finish()}ms`);

(async () => {
    try {
        await start();
    } catch (error) {
        log.error(error);
        process.exit(1);
    }
})();

// Application cleanup on exit
process.stdin.resume(); // So the program will not close instantly
function exitHandler (options, error) {
    if (options.cleanup) {
        console.log('Cleanup'); // eslint-disable-line no-console
    }
    if (error) {
        console.error(error.stack); // eslint-disable-line no-console
        process.exit(1);
    }
    if (options.exit) {
        process.exit(0);
    }
}
// Do something when app is closing
process.on('exit', exitHandler.bind(null, {
    cleanup: true
}));
// Catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {
    exit: true
}));
// Catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {
    exit: true
}));
